# C-Healer

C-Healer Reports

## Info

| Play Store     | https://play.google.com/store/apps/details?id=io.dotquantum.ch |
|----------------|----------------------------------------------------------------|
| Website        | https://covidhealer.org/                                       |
| Privacy Policy | https://covidhealer.org/termini-duso-e-privacy-policy/         |

***

## Reports

- 20220205 : Release 1.0.30 - Build #30

| Reference     | Link                                                                                                 |
|---------------|------------------------------------------------------------------------------------------------------|
| VirusTotal    | https://www.virustotal.com/gui/file/25e076ec7a9c5120b3fc31d69e85ddb6860786e375584788c341b5f00239ae9a |
| Apklab        | https://apklab.io/apk.html?hash=25e076ec7a9c5120b3fc31d69e85ddb6860786e375584788c341b5f00239ae9a     |
| Pithus        | https://beta.pithus.org/report/25e076ec7a9c5120b3fc31d69e85ddb6860786e375584788c341b5f00239ae9a      |
| JoeSandbox #1 | https://www.joesandbox.com/analysis/568114/0/html |
| JoeSandbox #2 | - |
| Intezer       | https://analyze.intezer.com/analyses/a1147e45-d760-4748-9b5a-d87f6689431f                            |
| Triage        | https://tria.ge/220208-as87bsbbg8   | 

- 20220112 : Release 1.0.22 - Build #22

| Reference     | Link                                                                                                 |
|---------------|------------------------------------------------------------------------------------------------------|
| VirusTotal    | https://www.virustotal.com/gui/file/4818179b099e41613a58d0c5eaa7096d0fb702bc87a1c827567e2258ef177f98 |
| Apklab        | https://apklab.io/apk.html?hash=4818179b099e41613a58d0c5eaa7096d0fb702bc87a1c827567e2258ef177f98     |
| Pithus        | https://beta.pithus.org/report/4818179b099e41613a58d0c5eaa7096d0fb702bc87a1c827567e2258ef177f98      |
| JoeSandbox #1 | https://www.joesandbox.com/analysis/546983/0/html                                                    |
| JoeSandbox #2 | https://www.joesandbox.com/analysis/546983/1/html                                                    |
| Intezer       | https://analyze.intezer.com/analyses/23abc08f-59c2-4617-b736-0680653e7b41                            |
| Triage        | https://tria.ge/220112-xmj2ksdgak   | 

- 20211231 : Release 1.0.18 - Build #18

| Reference     | Link                                                                                                 |
|---------------|------------------------------------------------------------------------------------------------------|
| VirusTotal    | https://www.virustotal.com/gui/file/b2047eb3a60d80ace128eecbf6ee813a180c5051ea7e9d3851f1ff1f67c13d47 |
| Apklab        | -     |
| Pithus        | https://beta.pithus.org/report/b2047eb3a60d80ace128eecbf6ee813a180c5051ea7e9d3851f1ff1f67c13d47      |
| JoeSandbox #1 | https://www.joesandbox.com/analysis/552019/0/html                                                    |
| JoeSandbox #2 | https://www.joesandbox.com/analysis/552019/1/html                                                    |
| Intezer       | https://analyze.intezer.com/analyses/9803f87b-87ce-4ca3-93b7-1ccdf36422de                            |
| Triage        | https://tria.ge/220101-xt1ynsgdbj   | 

- 20211222 : Release 1.0.17 - Build #17

| Reference     | Link                                                                                                 |
|---------------|------------------------------------------------------------------------------------------------------|
| VirusTotal    | https://www.virustotal.com/gui/file/9fb8a3cfb755d5df78c363ddbb83092db34aed69cca39ebf6bb9af9984571a67 |
| Apklab        | https://apklab.io/apk.html?hash=9fb8a3cfb755d5df78c363ddbb83092db34aed69cca39ebf6bb9af9984571a67     |
| Pithus        | https://beta.pithus.org/report/9fb8a3cfb755d5df78c363ddbb83092db34aed69cca39ebf6bb9af9984571a67      |
| JoeSandbox #1 | https://www.joesandbox.com/analysis/545209/0/html                                                    |
| JoeSandbox #2 | https://www.joesandbox.com/analysis/545209/1/html                                                    |
| Intezer       | https://analyze.intezer.com/analyses/7728a5b4-ff05-4916-be29-a6d610025ce2                            |
| Triage        | https://tria.ge/211225-nfkj4agfer                                                                    |

## Endpoints

- Basic Firebase app

| Host                                 | Path                                            | Params | Scope                      |
|--------------------------------------|-------------------------------------------------|--------|----------------------------|
| firebaseinstallations.googleapis.com | /v1/projects/covid-healer-prod/installations    |  -     | response with FID & tokens |
| securetoken.googleapis.com           | /v1/token                                       | key    | access & refresh tokens    |
| www.googleapis.com                   | /identitytoolkit/v3/relyingparty/getAccountInfo | key    | account IDs & session info | 
| covid-healer-prod.firebaseapp.com    | - OMISSIS -                                     |  -     | auth / admin / hosting     |
| us-central1-covid-healer-prod.cloudfunctions.net | - OMISSIS -                         |  -     | APIs & services            |

## Email Sender

- Account Confirmation email messages - managed & sent through [Mailgun](https://www.mailgun.com/) services. Samples of sender Mailgun hosts-info (from the headers of confirmation messages) with related CIDR/Net ranges.

| Host | m204-229.eu.mailgun.net | 
|------|-------------------------| 
| IP   | 161.38.204.229          |
| CIDR | 161.38.192.0/20         | 

| Host | m239-4.eu.mailgun.net   | 
|------|-------------------------| 
| IP   | 185.250.239.4           | 
| CIDR | 185.250.236.0/22        |

## Privacy / Data Collection

- Tracking details (Google Firebase Analytics + app plugins' metrics) available from [Pithus Report](https://beta.pithus.org/report/25e076ec7a9c5120b3fc31d69e85ddb6860786e375584788c341b5f00239ae9a) - tabs Behavior Analysis & Control Flow Analysis. _Notice : Google Firebase Analytics - Flutter plugin code has just been relocated (not deleted) since release 1.0.18 (build 18)_ .

- Since release 1.0.22 (build 22) the `com.google.android.gms.permission.AD_ID` permission declaration has been added into the app's AndroidManifest. Such permission addition is related to the [**Google Play Advertising ID (AAID)**](https://support.google.com/googleplay/android-developer/answer/6048248?hl=en) - **NOT needed for apps with target API level set to 30 - Android 11/R (_current targetSdk level of the app_) or older, unless they retrieve the Advertising ID through the [Advertising ID API](https://developers.google.com/android/reference/com/google/android/gms/ads/identifier/AdvertisingIdClient) for ads/advertising purposes**.

- There is no in-app OPT-IN for ads purposes. Therefore **app users can only set the OPT-OUT of Interest-based Advertising / Ads Personalization into their Android device settings - Google (Services & Preferences) \ Ads section**. 

- Screencapture support for Flutter plugin logs (check JoeSandbox reports)

- Onboarding process -> check the [**screenshots**](https://gitlab.com/rawmain/c-healer/-/commit/c728fd3508f14291f73561eb60ff9a5de89668f5) . There is also a [video-tutorial available on Youtube for doctors' signup process](https://www.youtube.com/watch?v=m4Xg4Tb9r2Q) .

Account creation request

| Signup Form Data | Compulsory | User | Doctor |
|------------------|------------|------|--------|
| Name             | Yes        | X    | X      |
| Surname          | Yes        | X    | X      |
| Email Address    | Yes        | X    | X      |
| Password         | Yes        | X    | X      |

After Email confirmation

| Signup Form Data          | Compulsory | User | Doctor |
|---------------------------|------------|------|--------|
| Face Photo                | Yes        | X    | X      |    
| Birthdate                 | Yes        | X    | X      | 
| Gender                    | Yes        | X    | X      |   
| Street Address            | Yes        | X    | X      |   
| City/Town                 | Yes        | X    | X      |
| Province                  | Yes        | X    | X      | 
| ZIP Code                  | Yes        | X    | X      | 
| Country                   | Yes        | X    | X      |  
| ID Photo                  | Yes        | X    | X      |

| Signup Form Data          | Compulsory | User | Doctor |
|---------------------------|------------|------|--------|
| Order Registration Number | Yes        |      | X      |    
| Membership Group          | No         |      | X      |    
| Order Card Photo          | Yes        |      | X      |    
| Drug Book Title           | Yes        |      | X      |    
| Drug Book Category        | Yes        |      | X      | 
| Drug Book Specialization  | No         |      | X      | 
| Drug Book Street Address  | Yes        |      | X      | 
| Drug Book City/Town       | Yes        |      | X      | 
| Drug Book Province        | Yes        |      | X      | 
| Drug Book ZIP Code        | Yes        |      | X      | 
| Drug Book Country         | Yes        |      | X      |
| Drug Book Email Address   | No         |      | X      | 
| Drug Book Phone Number    | No         |      | X      | 
| Drug Book Signature Photo | Yes        |      | X      | 

- Once patient-profile has been manually validated, the user must send by email a filled & signed PDF form. Otherwise, the app can't be used yet.

| Additional Consent Form Page | https://covidhealer.org/consenso-informato/ |
|------------------------------|---------------------------------------------|
| Additional Consent Form PDF  | https://covidhealer.org/wp-content/uploads/2021/12/Consenso_informato-1.pdf |
  
--------------------------

> _**Side notice :**_
>
> _**Release 1.0.30 build #30 (published on February 5th, 2022) of C-Healer app doesn't allow new users to sign up**._
>
> _**Formerly registered users can still log in without any issue**, but new users get an alert-dialog when they tap on the signup link._
>
> _Prior releases of C-Healer app are still working & even allow new users to perform the first part of the onboarding process in order to get the confirmation email. However, **once the new users click on the confirmation link, the old release will stop the onboarding process & request them to update the app**._
>
> | Release 1.0.30 Alert Dialog | Release 1.0.22 Update Request |
> |------------------------------|---------------------------------------------|
> | ![alt text](doc_img/1030_halted_registration.png "Release 1.0.30 - Halted registration")  | ![alt text](doc_img/1022_update_request.png "Release 1.0.22 - Update Request") |
